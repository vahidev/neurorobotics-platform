README
======

The Neurorobotics Platform (NRP) is a subproject of the Human Brain Project (HBP), funded by the European Commission (EC). It is distributed as open source under the terms of the GNU Public License version 2, which you can find a copy of in each of our repositories.

Visit our [website](http://www.neurorobotics.net)!

You also might want to browse the Neurorobotics Platform's [guide book](https://developer.humanbrainproject.eu/docs/projects/HBP%20Neurorobotics%20Platform/2.0/index.html) to discover its features.

## What is this repository for? ##

This is the meta-repository for the Neurorobotics Platform. It provides the guide to installation of the Platform from source code.

## Who do I talk to? ##

To report a bug or request a new feature, please send an email to [neurorobotics-support@humanbrainproject.eu](mailto:neurorobotics-support@humanbrainproject.eu) or, if you have HBP credentials, log in to the [HBP support portal](https://support.humanbrainproject.eu/).

To get support from the developer team, post on our [forum](https://forum.humanbrainproject.eu/c/neurorobotics).

In case you need to contact us for another purpose, please use the [contact form](http://www.neurorobotics.net/contact.html).

## Upgrading from Ubuntu 16 to Ubuntu 18 ##

  **Important note**: if you have a running NRP in Ubuntu 16, your **version is frozen to 2.3** and you will **not get any new updates**. Consider upgrading to Ubuntu 18. The following link provides hints on how to upgrade:
  
  [Ubuntu 18 upgrade guide](https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/src/master/ubuntu18_upgrade.md)

  If you install a fresh NRP on Ubuntu 18, just read on.

## Source code full NRP installation ##

### Supported OS ###
This installation doc has been tested on **Ubuntu 16.04** and **Ubuntu 18.04**. If using a different distribution, you will have to adapt the dependencies without support from us. Installations on **virtual machines**
are often failing due to bad support of gpu drivers, but you can configure NRP to use CPU-rendering instead. This will slow it down, but should work.

---
**Ubuntu 16.04 updates (as of March 2019) may break python paths**

  When installing a fresh Ubuntu 16.04, *do not* select the automatically update checkbox before installation starts. This leads to broken python paths that are hardly recoverable.
  
---
**WARNING**

  Apt and pip are not good friends. If you installed many dist-packages using sudo pip install, you should consider uninstalling them and using the deb packages provided by Ubuntu instead, even if the versions are late. Indeed, the deb packages are tested by Ubuntu and work together. They are the reference. Anything else is installed locally at build time.
  So, practically, you should have a $HOME/.local/lib/python2.7/dist-packages containing ONLY Nest related stuff: nest, PyNest, Topology and ConnPlotter. If you have other packages, that exist as deb packages, you are strongly advised to pip uninstall them and apt-get install the deb versions instead.
  In particular, pip should be the official Ubuntu one, and not be pip installed.
  
    pip uninstall pip
    sudo apt-get install python-pip

  PLEASE: see known issues and troubleshooting and the end of the page!
---

This documentation helps you install a fully local neurorobotics platform. You can of course choose to install all the backend stuff on one computer and the frontend on your laptop for example also, but this is not described here.
**Very important**: in this setup, NRP is installed in your user space (no root install).

NRP Installation
================

## 1. Initial set up

1. Create a folder in you home which will contain required 3rd-party software

        mkdir $HOME/.local

2. Create a new directory where you will install all the source code for NRP. We suggest $HOME/Documents/NRP. This directory will be referred to in the rest of this document as **$HBP**.
    Add the `$HBP` environment variable to your bashrc.
    Add the `$NRP_INSTALL_MODE` variable to your bashrc which will make sure you use the publicly accessible code and build process, **unless you are a core developer and have writing rights to the repos**.

        # add the following to your $HOME/.bashrc file
        export HBP=$HOME/Documents/NRP  # Or whatever installation path you chose
        export NRP_INSTALL_MODE=user  # skip this variable if you are a core NRP developer and want the developer branches of the repos

    Source your .bashrc

        source $HOME/.bashrc

3. Apt prerequisites

         sudo apt-get install cmake git build-essential doxygen python-dev python-h5py python-lxml autogen automake libtool build-essential autoconf libltdl7-dev libtinyxml-dev libreadline6-dev libncurses5-dev libgsl0-dev python-all-dev python-docopt python-numpy python-scipy python-matplotlib ipython libxslt1-dev zlib1g-dev libfreetype6-dev python-opencv ruby libtar-dev libprotoc-dev protobuf-compiler imagemagick libtinyxml2-dev python-virtualenv libffi-dev uwsgi-plugin-python python-pip cimg-dev libgnuplot-iostream-dev
 
     Install ipython notebook:
 
         # On Ubuntu 16.04 xenial
         sudo apt-get install ipython-notebook
         # On Ubuntu 18.04 bionic
         sudo apt-get install jupyter-notebook

4. Clone the `user-scripts` repository. This repository includes some helper scripts to ease the installation.

        mkdir -p $HBP
        cd $HBP
         # On Ubuntu 16.04 xenial
        git clone https://bitbucket.org/hbpneurorobotics/user-scripts.git --branch=master
         # On Ubuntu 18.04 bionic
        git clone https://bitbucket.org/hbpneurorobotics/user-scripts.git --branch=master18

5. Use the clone-all-repos script from user-scripts to clone all necessary repos at once. The $HBP variable need to be set for this script to work successfully!

        cd $HBP/user-scripts
        ./clone-all-repos

6. Empty the PYTHONPATH env variable. Add the following to the end of your ~/.bashrc

        PYTHONPATH=

7. Set up your environment automatically by adding the following two lines at the Bottom of your .bashrc

        . $HBP/user-scripts/nrp_variables
        . $HBP/user-scripts/nrp_aliases

    **Note:** Generally, if you want to change any of the variables for any reason, then override the variables in your .bashrc and do not alter the nrp_variables file.
    
    Then source you bashrc. **Note** that you will get some expected errors after sourcing the bashrc if you're doing a fresh install. This is due to some missing files that will be instantiated during the installation process. 

        source $HOME/.bashrc

## 2. Install ROS ##

**Note**: if you already installed ROS for your Ubuntu from apt-get, then you can skip this section.

1. Add GPG key of ROS to apt

        sudo wget -O - https://raw.githubusercontent.com/ros/rosdistro/master/ros.key | sudo apt-key add -

2. Add apt repository for ROS

        # On Ubuntu 16.04 xenial
        sudo apt-add-repository "deb http://packages.ros.org/ros/ubuntu xenial main"
        # OR
        # On Ubuntu 18.04 bionic
        sudo apt-add-repository "deb http://packages.ros.org/ros/ubuntu bionic main"

3. Update repositories

        sudo apt-get update

4. Install ROS Kinetic for Ubuntu 16 or ROS Melodic for Ubuntu 18.

        # On Ubuntu 16.04 xenial
        sudo apt-get install ros-kinetic-desktop-full
        sudo apt-get install ros-kinetic-web-video-server
        sudo apt-get install ros-kinetic-control-toolbox ros-kinetic-controller-manager ros-kinetic-transmission-interface ros-kinetic-joint-limits-interface ros-kinetic-rosauth
        # OR
        # On Ubuntu 18.04 bionic
        sudo apt-get install ros-melodic-desktop-full
        sudo apt-get install ros-melodic-web-video-server
        sudo apt-get install ros-melodic-control-toolbox ros-melodic-controller-manager ros-melodic-transmission-interface ros-melodic-joint-limits-interface ros-melodic-rosauth ros-melodic-smach-ros python-rospkg

5. Initialize ROS

         sudo rosdep init
         rosdep update

## 3. Gazebo prerequisites

1. First, if you have Gazebo 6 already installed, remove it first. (Remove any other version of Gazebo that you would have installed by yourself)

        # On Ubuntu 16.04 xenial (ROS Kinetic)
        sudo apt-get remove --purge gazebo6* ros-kinetic-gazebo6-msgs ros-kinetic-gazebo6-plugins ros-kinetic-gazebo6-ros ros-kinetic-gazebo6-ros-control ros-kinetic-gazebo6-ros-pkgs
        # OR
        # On Ubuntu 18.04 bionic (ROS Melodic)
        sudo apt-get remove --purge gazebo9* ros-melodic-gazebo9-msgs ros-melodic-gazebo9-plugins ros-melodic-gazebo9-ros ros-melodic-gazebo9-ros-control ros-melodic-gazebo9-ros-pkgs
        

    Test that roscore is still functioning. If not, consider reinstalling ROS by first removing it with apt-get remove --purge ros-<melodic/kinetic>* and following the above paragraph again.

2. Add GPG key of Gazebo to apt

        wget http://packages.osrfoundation.org/gazebo.key -O - | sudo apt-key add - 

3. Add apt repository for Gazebo
    	
        sudo sh -c 'echo "deb http://packages.osrfoundation.org/gazebo/ubuntu-stable `lsb_release -cs` main" > /etc/apt/sources.list.d/gazebo-stable.list'

4. Update repositories
    	
        sudo apt-get update

5. Install build dependencies

        # On Ubuntu 16.04 xenial (ROS Kinetic)
    	sudo apt-get install libignition-math2-dev libignition-transport-dev libignition-transport0-dev
        # OR
        # On Ubuntu 18.04 bionic (ROS Melodic)
        sudo apt-get install libignition-math6-dev libignition-transport-dev libignition-transport4-dev

6. Remove previous bullet physics engine

    Remove previous bullet packages with apt-get remove (libbullet, ...).

7. Install bullet from apt

    sudo apt-get install libbullet-dev

8. Install Gazebo dependencies

    You may require additional libraries to compile gazebo with all its options:
    	
        sudo apt-get install libgts-dev libgdal-dev ruby-ronn xsltproc graphviz-dev

    **Warning**: Check that $HOME/.local/lib/x86_64-linux-gnu/cmake/gazebo is in your $CMAKE_PREFIX_PATH. If not, you might have forgotten to add nrp_variables in your .bashrc.
    Further, make sure that $HOME/.local/lib/x86_64-linux-gnu/pkgconfig is in your $PKG_CONFIG_PATH. Otherwise, you will see errors that later builds will claim they did not find the HBP Gazebo features.

    **Warning**: When you will build the whole NRP later, if gazebo build fails because you miss a number of libs (see end summary from cmake output), just apt-get the required ones, from the "BUILD_ERRORS" in the cmake step.

    **Note**: Check that $GAZEBO_MODEL_PATH for example exists. If not, you might have forgotten to add nrp_variables in your .bashrc.

## 4. NEST prerequisites

1. Remove any previous system level installations of PyNN
	
        sudo apt-get remove --purge python-pynn

2. Install the GSL developer package
	
        sudo apt-get install libgsl0-dev

3. Install Yacc (for the MPI layer)

        sudo apt-get install bison byacc

## 5. Gzweb prerequisites

1. First make sure that build dependencies are installed.

        sudo apt-get install libgts-dev libjansson-dev

2. Install nvm (Node Version Manager) and nodejs v8 (+ v0.10 for Ubuntu 16)

    Install nvm (steps below taken from https://github.com/creationix/nvm)

        curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash
        source ~/.bashrc # or reopen your shells

    Install node

        # **Only** Ubuntu 16
        nvm install 0.10

        # both Ubuntus
        nvm install 8
        nvm alias default 8

3. Install bower

        npm install -g bower

## 6. NRP prerequisites

1. Configure NRP

    Go to user-scripts and run the configure scipts. This will set up Makefiles and default config files.

        cd $HBP/user-scripts
        ./configure_nrp 2>/dev/null # you don't have to answer the questions so far, just wait 5 seconds

    ** Note **: If you get errors, like missing directories or files, this is fine, they will be fixed later in the process. Just ignore them and proceed.

2. Install dependencies

    Install these dependencies:
    
        sudo apt-get install python-pip python-virtualenv
        # remove standard swagger packages (we use custom ones from NRP dependencies)
        # remove the apt versions of these swagger packages too
        sudo pip uninstall swagger flask-swagger flask-restful-swagger

        # install some deps needed for then backend (scipy deps)
        sudo apt-get install libblas-dev liblapack-dev libhdf5-dev gfortran

## 7. Install Nginx

Nginx is a web server, just like apache, that is very modular and in our case serves all the traffic, adding an authentication layer. It acts as a port forwarder. The backend services that run on 5000 will be mapped on port 8080 to the outside, the assets will be served on 8040, the rosbridge websocket mapped on 9091, as well as the gzbridge websocket.

1. list existing nginx packages (```apt list --installed | grep nginx```) and remove if any

2. Install nginx-extras and lua-cjson

        sudo apt-get install nginx-extras lua-cjson

    Your setup should be finished with this step. The configuration of nginx is done automatically in a later step of the 100% install. Though, if you experience issues after you finish all the steps, go to the troubleshooting section below.

## 8. Frontend prerequisites

If you run into problems, see the Troubleshooting section at the end of the page.
    	
1. Install dependencies:

        cd $HBP # your NRP directory
        cd ExDFrontend
        sudo apt-get install ruby-compass
        sudo gem install compass

2. Install grunt:

        npm install -g grunt-cli
        npm install -g grunt

## 9. Build NRP

1. Open a **fresh** terminal to be certain to have the proper environment and run the configure_nrp script in user-scripts

        cd $HBP/user-scripts
        ./configure_nrp  # don't answer N to the question about creating a local database (if you answer nothing, it assumes Y)

    This script copies the configuration files of nginx, ExDFrontend, CLE as well as the gzserver & gzbridge scripts to the correct locations. It also modifies the raw config files to include the correct paths and usernames.

2. Build the Platform at once:

        ./update_nrp build all

    This will take time since all the python dependencies will be downloaded and numpy takes a while to build. If the script does not fail, then you are done.
    If the build fails, go to the troubleshooting sections in the relevant sections below.

3. **Virtual machines ONLY**: if you are installing on a virtual machine, or your GPU is not supported for any reason, you may use CPU rendering, which is slower, but always works. Just execute:

        ./rendering_mode cpu

    To switch back to GPU rendering, execute:
    
        ./rendering_mode gpu

Running the Platform
====================

Make sure nginx is running fine (restart it : it should output [ OK ] ) and watch the error_log.

        cle-nginx

Then start the NRP backend by using either

        cle-start

You can then connect with your browser:

Open
http://localhost:9000/#/esv-private
You will be prompted for credentials: default user is "nrpuser/password". Clone an experiment.

If you want to see experiments that are in "development" maturity (your own for example), add the "?dev" parameter to the url:
http://localhost:9000/#/esv-private?dev

Stopping the Platform
---------------------

To stop the Platform and kill all processes:

* press CTRL+C in the terminal where you entered "cle-start" and type
  
         cle-kill

Updating the Platform
=====================

This is very easy! Just go to ```user-scripts``` and run

        cd $HBP/user-scripts
        git pull --rebase
        ./update_nrp update all
        ./configure_nrp

And open a fresh terminal to start your Platform as usual.
If you don't provide the optional ```all``` argument, only the core python repos will be updated, which is a much shorter build, but might miss changes in 3rd party
software like gazebo. So we would recommend that you just use this argument and get a lunch break while it builds.

## Update error?

The update_nrp script will exit on error if you have local changes in the repos. So if you do not want to risk a failure after one hour of build, we recommend that you make sure your repos in $HBP are clean or changes are stashed or committed.

[issues]: https://bitbucket.org/hbpneurorobotics/neurorobotics-platform/issues

Troubleshooting
===============

## Troubleshoot ROS

---
**Possible bug**: If you upgraded from Ubuntu 14.04 to 16.04 you might experience the following error when installing ROS:

    dpkg: error processing package ros-kinetic-desktop-full (--configure):
    dependency problems - leaving unconfigured
    Processing triggers for libc-bin (2.23-0ubuntu3) ...
    Errors were encountered while processing:
      libopenni0
      libpcl-io1.7:amd64
      libpcl-visualization1.7:amd64
      libpcl1.7
      libopenni-sensor-pointclouds0
      ros-kinetic-pcl-conversions
      ros-kinetic-pcl-ros
      ros-kinetic-perception-pcl
      openni-utils
      libopenni-dev
      libpcl-dev
      ros-kinetic-perception
      ros-kinetic-desktop-full
This issue can be solved by purging all libopenni packages:

    $ sudo apt-get purge libopenni*
And then try installing ROS again. The libopenni packages will be automatically reinstalled.

---


## Troubleshoot Gazebo

#### anaconda conflict breaks the build

If you get build errors in gazebo in the python TIFF libraries, you might have a conflict with anaconda.

    undefined symbol TIFFIsTiled@LIBTIFF_4.0

You have to get anaconda out of the PATH by resetting PATH temporarily at the end of your .bashrc, rebuild the NRP, and finally clean up your .bashrc.

#### gzserver segfault on start

    gzserver: /usr/include/boost/thread/pthread/recursive_mutex.hpp:101: boost::recursive_mutex::~recursive_mutex(): Assertion `!pthread_mutex_destroy(&m)' failed.

*Solution*: Try to launch the gzserver binary with no argument. 
If it runs, it probably means that you made something wrong when compiling GazeboRosPackages. 
Make sure that you didn't source another catkin workspace before you built GazeboRosPackages.

#### gazebo cmake error (happened with AMD GPU):

    CMake Error at cmake/CheckDRIDisplay.cmake:54 (STRING):
    string sub-command FIND requires 3 or 4 parameters.
    Call Stack (most recent call first):
    CMakeLists.txt:154 (include)

    --! valid dri display not found (no glxinfo or pyopengl)
    
*Solution*: install mesa-utils (glxinfo)

#### gazebo cmake error (missing tinyxml2): install libtinyxml2-dev.

#### gazebo libccd link error (/usr/bin/ld: cannot find -lccd):

You likely have a conflicting version of ros-indigo-libccd installed (from other ros packages such as MoveIt).
*Solution*: edit cmake/SearchForStuff.cmake and replace "pkg_check_modules(CCD ccd>=1.4)" with "SET(CCD_FOUND FALSE)"
delete your build directory, rerun cmake, and build as usual

## Troubleshoot Nest

Come back here after NRP build if you experience issues with Nest.

---
**2017.12.13 note : If Nest build fails**

  We changed the version of Nest to 2.12. This version uses a different build system. We tried to have an as smooth as possible update process, but the update_nrp script might have trouble building Nest.
  In that case, the easiest is to save your local Nest changes (don't stash, really copy then somewhere else), and to reclone the repo and redo the build steps.
    
        cd $HBP
        rm -rf nest-simulator
        git clone https://bitbucket.org/hbpneurorobotics/nest-simulator.git

  Then just either relaunch "update_nrp build all" or run the Nest build steps manually on your freshly cloned repo.

---

---
**Possible Bug**

NEST creates broken symbolic links in $HOME/.local/lib/python2.7/dist-packages/PyNEST*.egg-info and $HOME/.local/lib/python2.7/dist-packages/ConnPlotter*.egg-info
Either delete the symlinks or fix them. In case you delete them, make sure to escape the * in the delete command or you'll delete the actual library.
Ex: ```$ sudo rm PyNEST\*.egg-info ConnPlotter\*.egg-info```

---

---
**Warning**: If you see an error like: ```error: command 'x86_64-linux-gnu-gcc' failed with exit status 1```
and then further up is : ```libhdf5.so: cannot open shared object file: No such file or directory```
Check if you have libhdf5-dev installed. If not, install it.
Then check that /usr/lib/x86_64-linux-gnu is in your $LD_LIBRARY_PATH (should if you correctly include $HBP/user-scripts/nrp_variables in your .bashrc).

---

---
**Build failing**

If the build is completely failing (this might happen if Nest is being updated from 2.10 to 2.12), then save your local Nest changes (don't stash, really copy them
somewhere else), reclone the repo and redo the build steps.
    
    cd $HBP
    rm -rf nest-simulator
    git clone https://$user@bitbucket.org/hbpneurorobotics/nest-simulator.git

---

## Troubleshoot Gazebo Plugins (GazeboRosPackages)

Come back here if the NRP build fails on GazeboRosPackages.

**Warning**: To avoid compiling against gazebo2 embedded with ros-indigo-desktop-full, make sure you prepend CMAKE_PREFIX_PATH so it points to NRP's gazebo-config.cmake in $HOME/.local/... (check that ```$CMAKE_PREFIX_PATH``` contains  ```$HOME/.local/lib/x86_64-linux-gnu/cmake/gazebo/```, which should be the case if you included nrp_variables in your bashrc).

**Warning**: If GazeboRosPackages build fails, it might be that the ROS setup file has not be sourced. If you added nrp_variables correctly in your bashrc, this should not be the case.
Else, open a fresh terminal, and check that $ROS_MASTER_URI is set. If not you can manually source the setup and redo the ```catkin_make```.

    source /opt/ros/melodic/setup.bash # for Ubuntu 18
    source /opt/ros/kinetic/setup.bash # for Ubuntu 16
    catkin_make

**Note**: On Ubuntu 16 building may complain about some missing ROS modules. Just install the missing modules with apt-get install and re-build. i.e 

    sudo apt-get install ros-<version(kinetic or melodic)>-<missing package name>

## Troubleshoot compiling gzweb

Come back here if NRP build fails on gzweb.

To make sure that gzweb is compiled with proper Gazebo version, prepend the correct path to package configuration:

    export PKG_CONFIG_PATH=$HOME/.local/lib/x86_64-linux-gnu/pkgconfig:$PKG_CONFIG_PATH

If you get node-gyp errors ("File not found"), try deleting $HOME/.npm and re-running deploy script.

You may encounter:

    /usr/bin/env: node: No such file or directory
    There are node-gyp build errors, exiting.

This is fixed by a symbolic link: ```ln -s `which nodejs` $HOME/.local/bin/node```

You may also encounter core dump or segmentation faults depending on your graphics card and configuration. Install the proprietary drivers (not the X.org default driver) on your system and reboot.

You can see more possible problems and solutions here: http://gazebosim.org/tutorials?tut=gzweb_install&ver=1.9%2B&cat=gzweb#Troubleshooting

## Troubleshoot Nginx

If you are having issues with nginx configuration that could mean that your nginx version is too recent.
Try running

    nginx -t -c $HOME/.local/etc/nginx/nginx.conf

In case the output of '' is something like 'unknown directive "more_set_headers"', try replacing the following lines in $HOME/.local/etc/nginx/conf.d/gzweb.conf

        location / {
        ...
        #more_set_headers 'Access-Control-Allow-Origin: $cors_origin';
        #more_set_headers 'Access-Control-Allow-Methods: $cors_methods';
        #more_set_headers 'Access-Control-Allow-Headers: $cors_headers';
        #more_set_headers 'Access-Control-Expose-Headers: Content-Length';
 
        add_header 'Access-Control-Allow-Origin' '$cors_origin' 'always';
        add_header 'Access-Control-Allow-Methods' '$cors_methods' 'always';
        add_header 'Access-Control-Allow-Headers' '$cors_headers' 'always';
        add_header 'Access-Control-Expose-Headers' 'Content-Length' 'always';
        ...
        }

and in nrp-services.conf

        location / {
                ...
                #more_set_headers 'Access-Control-Allow-Origin: $cors_origin';
                #more_set_headers 'Access-Control-Allow-Methods: $cors_methods';
                #more_set_headers 'Access-Control-Allow-Headers: $cors_headers';
 
                add_header 'Access-Control-Allow-Origin' '$cors_origin' 'always';
                add_header 'Access-Control-Allow-Methods' '$cors_methods' 'always';
                add_header 'Access-Control-Allow-Headers' '$cors_headers' 'always';
                ...
        }
 
        location /api {
                ...
                #more_set_headers 'Access-Control-Allow-Origin: $cors_origin';
                #more_set_headers 'Access-Control-Allow-Methods: $cors_methods';
                #more_set_headers 'Access-Control-Allow-Headers: $cors_headers';
 
                add_header 'Access-Control-Allow-Origin' '$cors_origin' 'always';
                add_header 'Access-Control-Allow-Methods' '$cors_methods' 'always';
                add_header 'Access-Control-Allow-Headers' '$cors_headers' 'always';
                ...
        }

nginx may complain that /<assets_root>/sdk does not exist. If so, just create an empty directory. No idea what is it for...

Use ```nginx -t -c $HOME/.local/etc/nginx/nginx.conf``` to get debug info if ```$HOME/.local/etc/init.d/nginx restart" returns [fail]```.

## Troubleshoot ExDFrontend

Come back here if NRP build fails on ExDFrontend or cle-frontend fails to run.

- Error when running npm install.
    If you see the following error:

        sh: node command not found

    you could try installing the package nodejs-legacy instead of nodejs:

        sudo apt-get install nodejs-legacy

- If you get this kind of JavaScript error, as reported in this post on the HBP forum, you may want to deactivate AdBlocker.

            Failed to instantiate module exdFrontendApp
            Error: [$injector:modulerr] Failed to instantiate module exdFrontendApp
            due to:
            [$injector:modulerr] Failed to instantiate module
            angulartics.google.analytics due to:
            [$injector:nomod] Module 'angulartics.google.analytics' is not
            available! You either misspelled the module name or forgot to load it.
            If registering a module ensure that you specify the dependencies as the
            second argument.
            http://errors.angularjs.org/1.4.8/$injector/nomod?p0=angulartics.google.analytics
            minErr/<@http://10.162.242.194:9000/bower_components/angular/angular.js:68:12
            module/<@h ttp://10.162.242.194:9000/bower_components/angular/angular.js:2005:
            
## Other troubles

#### ImportError: No module named _internal

This error comes when pip is updated with pip itself and has version >= 9. Solve it by uninstalling pipped pip and reinstalling Ubuntu's official one:

    pip uninstall pip
    sudo apt-get install python-pip
    pip -V

You should get

    pip 8.1.1 from /usr/lib/python2.7/dist-packages (python 2.7)

Original forum post about this is https://forum.humanbrainproject.eu/t/build-error-from-pip--internal-importerror-no-module-named--internal/567

#### File not found error on building brainvisualizer

If you get this kind of error and the build breaks in brainvosualizer:

    npm ERR! enoent ENOENT: no such file or directory, rename '/home/vonarnim/Documents/NRP/brainvisualizer/node_modules/npm/node_modules/ansistyles' -> '/home/vonarnim/Documents/NRP/brainvisualizer/node_modules/npm/node_modules/.ansistyles.DELETE'

then the solution is to manually remove the node_modules directory and rebuild

    cd $HBP/brainvisualizer
    rm -rf node_modules
    cd $HBP/user-scripts
    ./update_nrp build # resumes build (without third party that were successfully built before)
    ./configure_nrp
    
#### git is complaning about changed package.json files that it has to stash

Just type enter to stash them and forget about the issue. It is known and we will fix it but it is not harmful to your build.

#### undefined symbol: ompi_mpi_logical8 OR No module named pyNN.nest

If cle-start fails with the following error:

    Error starting the simulation. (service [/ros_cle_simulation/create_new_simulation] responded with an error:
    error processing request: /home/florian/.opt/platform_venv/lib/python2.7/site-packages/mpi4py/MPI.so: undefined symbol: ompi_mpi_logical8) (host: localhost)

or this related error:

    ImportError: No module named pyNN.nest

then you have a problem with your PYTHONPATH. Most probably your PYTHONPATH is containing /usr/lib/python2.7/dist-packages before the NRP paths. Try to erase it in your .bashrc
before loading nrp_variables.

    [...]
    export PYTHONPATH=
    . $HBP/user-scripts/nrp_variables
    [...]

If this does not help, the following (not very nice) solution has been reported. Before cle-start:

    source $HBP_VIRTUAL_ENV/bin/activate

which "forces" the platform_venv directory to the front of the Python search path.

#### cannot import name config

After launching an experiment, it might fail saying cannot import name config.

        pip install config

#### 502 error because of retina

If cle-start fails with 502, possibly the pyretina is missing. Check the install commands of "retina" in the $HBP/user-scripts/nrp_functions script. Try execute them by hand.

#### Nginx is not starting during system boot? Add the following lines to /etc/rc.local:

    if [ -f $HOME/.local/etc/init.d/nginx ]; then
        $HOME/.local/etc/init.d/nginx restart
    fi

#### general access problems

-> file not accessible? 404? Check that the whole path to the file is readable and/or writable to you.

#### not loading experiment list

-> cors problems? Install CORS in your browser (google that)

-> nginx: sudo killall and service restart

#### python DictError about some dictionary word missing

-> you probably run a local version of Flask. Check in /usr/local/lib/python2.7/dist-packages. There should be ONLY Nest related stuff. If there are pip pacakges, please remove them, especially Flask, SQLAlchemy, Werkzeug, itsdangerous and rerun $HOME/user-scripts/update_nrp build.

#### problems with gzweb deploy-*.sh

-> webify_models_v2.py: splits with mesh, do not put/use mesh in model names...

-> server crashes with error "Unable to find local grunt" or other nodejs package related error

    cd $HBP/ExDFrontend
    npm install

#### 'cle-start' / 'cle-rosbridge' complains about missing rosauth.srv

    sudo apt-get install ros-indigo-rosauth

#### 'cle-start' / 'cle-rosbridge' can't find package rosbridge_server. Add the following to .bashrc

    export ROS_PACKAGE_PATH=$ROS_PACKAGE_PATH:</path/to/GazeboRosPackages>/src

#### 'cle-start': backend can't find modules (e.g. ImportError: No module named flask_restful_swagger)

There was an error during make devinstall when running $HOME/user-scripts/update_nrp build. Then run "make devinstall" in ExDBackend as follows:

    make devinstall > devinstall-log.txt 2>&1

to see what happened. In the case of flask_restful_swagger, it might help to

- rename ExDBackend/hbp-flask-restful-swagger-master temporarily
- rerun make devinstall

Don't forget to restore ExDBackend/hbp-flask-restful-swagger-master to its original name afterwards.

#### 'cle-start': Failed to execute request http://localhost:8080/experiment. ERROR: Error: Error: connect ECONNREFUSED

Nginx is not running or not running correctly. Double check nginx is running and that there is no errors in the nginx logs.

#### Gazebo6 general problems or compiling GazeboRosPackages

to make sure there are no dependencies problems:

    sudo apt-get purge gazebo4 # or 2 or 6
    sudo apt-get remove gazebo4 # or 2 or 6
    sudo apt-get install ros-indigo-desktop-full

then clean build directories and rebuild:

    cd $HBP/GazeboRosPackages/
    rm -rf build
    catkin_make

it might be necessary to do 

    catkin_make --force-cmake

Building GazeboRosPackages failed for me with Gazebo installed from package repositories. (gazebo.physics.Joint::SetMappedRotationAxis not found) If that happens, remove gazebo packages and compile from EPFL sources as above.

If building Gazebo fails with the message

    Linking CXX shared library libgazebo_common.so
    /usr/bin/ld: /usr/local/lib/libavcodec.a(allcodecs.o): relocation R_X86_64_32 against `ff_h263_vaapi_hwaccel' can not be used when making a shared object; recompile with -fPIC
    /usr/local/lib/libavcodec.a: error adding symbols: Bad value
    collect2: error: ld returned 1 exit status

You can prevent it from building with ffmpeg support by editing $HBP/gazebo/cmake/SearchForStuff.cmake. In line 391 change HAVE_FFMPEG TRUE to HAVE_FFMPEG FALSE. This will cause it to always assume that ffmpeg is not installed. Alternatively, you could compile ffmpeg from sources and create dynamic libraries instead of the static ones hat are installed from the package management.

If a similar error comes up later during the compile referencing BulletPhysics, try recompiling Bullet:
First, delete the old Bullet files: Bullet does not provide a make uninstall target, so we need to do it manually. In the build folder, run

    sudo xargs rm < install_manifest.txt

This will remove all files installed by make install. Then recompile and give the flag -DBUILD_SHARED_LIBS=true to cmake. This will build dynamically linked libraries. If you do this, make sure both $HOME/.local/lib and $HOME/.local/lib/x86_64-linux-gnu is on your LD_LIBRARY_PATH. The recommended way of adding it is creating the file /etc/ld.so.conf.d/gazebo.conf with the content

    $HOME/.local/lib/x86_64-linux-gnu
    $HOME/.local/lib

and running sudo ldconfig

If an error similar to this comes up

    gazebo/physics/libgazebo_physics.so.6.0.6.hbp.1.0.0: undefined reference
    to `gazebo::sensors::SensorManager::SensorsInitialized()

check your version of cmake, if it is version 3, try and downgrade it to version 2 and see if it helps

e.g.

    apt list --installed | grep cmake # check the version of cmake
    sudo apt-get install cmake=2.8.12.2-0ubuntu3 cmake-data=2.8.12.2-0ubuntu3 # or just downgrade to a previous version

#### ImportError: No module named pyretina

Follow the tutorial here: Install Retina.

#### Toolbar buttons icons (play/stop) show up as strange characters

This issue is due to missing fonts and outdated dependencies. (see NRRPLT-2472)

- Clear browser cache and close the browser
- Update ruby (tested with 1.9.3p484)
- run "sudo gem install compass"

#### Robot is not moving even though spike monitor works, no errors

Run catkin_make in GazeboRosPackages.
