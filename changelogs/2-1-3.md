Version 2.1.3
=============

Compatibility break
-------------------

**Symptom:** your experiments are loading but not working anymore. Transfer functions are doing nothing and no data is coming from ROS topics.

**Reason:** ROS topic names for (almost) all previsously cloned experiments are not valid anymore. Starting from CLE v2.1.2, robots are added into scene under their own 'robot id' namespaces. Gazebo therefore prepends robot id to the topic names.

**How to fix:** in all your experiments, you have to prepend the ROS topics names with the robot ID they refer to.

For the following file changes, you can browse your files from the "Experiment files" tab in the NRP home view, download the relevant files, do the changes and upload them again with the same file name (to overwrite).

- Check and update your .exc experiment file

The robotPose tag now has a robotId attribute that you have to set as follows:

        <robotPose robotId="kevin" x="0.0" y="0.0" z="0.5" roll="0.0" pitch="-0.0" yaw="3.14159265359" />

- Check and update your .bibi experiment file

The bodyModel tag now has a robotId attribute that you have to set as follows:

        <ns1:bodyModel robotId="kevin">husky_model/model.sdf</ns1:bodyModel>

- Change your transfer functions' topic names by prepending the robotId

Launch your simulation, and in the Transfer Functions editor, change your functions as follows:

        @nrp.MapRobotSubscriber("camera", Topic('/husky/camera', sensor_msg.msg.Image))

to be changed to:

        @nrp.MapRobotSubscriber("camera", Topic('/kevin/husky/camera', sensor_msg.msg.Image))

Where 'kevin' is the robot ID you give to your robot in the .exc and .bibi files.

These two screenshots show you these changes from within the frontend interface.

Set the robot ID in EXC file:
![Set robot ID](images/2-1-3-2.png)

Change topic names in transfer functions:
![Change topic names](images/2-1-3-1.png)
